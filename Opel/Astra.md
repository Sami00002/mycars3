Viteza maxima: 185 km / h
Overclocking Timp de până la 100 km / h: 12.3 C.
Consumul de combustibil 100 km în oraș: 8,5 L.
Consumul de combustibil 100 km pe autostradă: 5.5 L.
Consumul de combustibil de 100 km în ciclu mixt: 6,6 L.
Volumul rezervorului de gaz: 52 L.
CURB Masina Greutate: 1265 kg.
Admisibilă masa completă.: 1740 kg.
Dimensiunea anvelopei: 195/65 R15 T.
Dimensiunea discului: 6.5J X 15.
Caracteristicile motorului
Locație: în față, în cruce
Volumul motorului: 1598 cm3.
Puterea motorului: 105 HP.
Numărul de revoluții: 6000
Cuplu: 150/3900 N * M
Sistem de alimentare: Injecția distribuită
Turbocharddv: nu
Mecanism de distribuție a gazelor: DOHC.
Cilindru Locul de amplasare: În linie
Numărul de cilindri: 4
Diametrul cilindrului: 79 mm.
Pistonul se mișcă: 81,5 mm.
Rata compresiei: 10.5
Numărul de supape pe cilindru: 4
Combustibil recomandat: AI-95.
Sistem de franare
Frâne frontale: Discul ventilat.
Frânele din spate: Disc
ABS: Abs.
Direcție
Tip de direcție: Rake Gear.
Servodirecție: Hydrauscitel.
Transmisie
Unitate de antrenare: Față
Numărul de unelte: Cutie manuală - 5
Numărul de unelte: Caseta automată - 5
Raportul de transmisie al perechii principale: 3.94
Suspensie
Suspensie frontală: Stand de depreciere
Suspensie spate: Stand de depreciere
Corp
Tipul corpului: Hatchback.
Numărul de uși: 5
Număr de locuri: 5
Lungimea mașinii: 4249 mm.
Lățimea mașinii: 1753 mm.
Înălțimea mașinii: 1460 mm.
Ampatament: 2614 mm.
Pista frontală: 1488 mm.
Pitch înapoi: 1488 mm.
Volumul trunchiului este maxim: 1330 L.
Volumul trunchiului este minim: 380 L.