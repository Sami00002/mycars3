Anul inceperii productiei 2006; Anul opririi productiei 2008 | Combi
Putere : de la 65 CP la 105 CP
Lungime 4473 mm (176.1 in.); Lăţime 1993 mm (78.46 in.); Înălţime 1674 mm (65.91 in.); Ampatament 2905 mm (114.37 in.);
1.6i 16V (105 CP) 2006 - 2008
Viteza maximă : 183 km/h | 113.71 mph
0-100 km/h: 10.2 sec, 0-60 mph: 9.7 sec
Consumul de combustibil: 7.5 l/100 km | 31 US mpg | 38 UK mpg | 13 km/l
